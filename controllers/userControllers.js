const User = require('../models/User');

const bcrypt = require('bcrypt');

const auth = require('../auth');

const Product = require('../models/Product');

// Registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		userName : reqBody.userName,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})

	return newUser.save().then((user, err) => {

		if (err) {

			return false

		} else {

			return user
		}
	})
}

// Log-in

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null) {

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}

// Get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result
	});
};

// Set as admin

module.exports.setUserAdmin = async (data) => {

	if (data.payload.isAdmin == true) {

		return User.findById(data.userId).then((result, err) => {

			result.isAdmin = true;

			return result.save().then((setUserAdmin, err) => {

				if(err) {

					return false;

				} else {

					return result;
				}
			})
		})
	} else {
		return false
	}
}

// module.exports.setUser = async (data) => {
// 	console.log(data);
// 	if (data.isAdmin === true) {
// 		return User.findOne({}).then((result, err) => {
// 			console.log(result)
// 			if (data.payload.isAdmin === true) {
// 				result.isAdmin = false;
// 				return result.save().then((updatedUser, err) => {
// 					if (err) {
// 						return false;
// 					} else {
// 						return result;
// 					}
// 				})
// 			}
// 		})
// 	} else {
// 		return false;
// 	}
// }


