const User = require('../models/User');

const bcrypt = require('bcrypt');

const auth = require('../auth');

const Product = require('../models/Product');

// Add a product

module.exports.addProduct = async (data) => {
console.log(data)
	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			productName : data.product.productName,
			description : data.product.description,
			price : data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation successful
			if (error) {

				return false;

			// Product creation failed
			} else {

				return `Product added ${product}`;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	
};

// retrieval of active courses

module.exports.getAllActive = () => {
	
	return Product.find({isActive : true}).then(result => {

		return result
	})
}

// retrieval of a specific product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result
	})
}

// updating a product

module.exports.updateProduct = (data) => {
	console.log(data);
	return Product.findById(data.productId).then((result,err) => {
		console.log(result)
		if(data.payload.isAdmin === true){
		
				result.productName = data.updatedProduct.productName
				result.description = data.updatedProduct.description
				result.price = data.updatedProduct.price
				
			console.log(result)
			return result.save().then((updatedProduct, err) => {
		
				if(err){
					return false
				} else {
					return updatedProduct
				}
			})
		} else {
			return false
		}
	})
}

// archiving a product

module.exports.archiveProduct = async (data) => {

	if(data.payload.isAdmin === true) {

		return Product.findById(data.productId).then((result, err) => {

			result.isActive = false;

			return result.save().then((archivedProduct, err) => {

				if(err) {

					return false;

				} else {

					return result;
				}
			})
		})

	} else {

		return false;
	}
}
