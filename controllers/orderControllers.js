const Order = require('../models/Order');

const bcrypt = require('bcrypt');

// Add a order

module.exports.addOrder = (reqBody) => {

	let newOrder = new Order({
		buyerName : reqBody.buyerName,
		totalAmount : reqBody.totalAmount,
		productBought : reqBody.productBought

	});

	return newOrder.save().then((order, err) => {

		if(err){
			return false;
		} else {
			return order;
		}
	})
}

// retrieving all orders (admin only)

module.exports.getAllOrders = async (user) => {

	if(user.isAdmin === true){

		return Order.find({}).then(result => {

			return result
		})

	} else {

		return `${user.email} is not authorized`
	}
}

// retrieval of a specific order

// module.exports.getOrder = (reqParams) => {

// 	return Order.findById(reqParams.orderId).then(result => {

// 		return result
// 	})
// }

// *
module.exports.getOrder = (payload) => {

	return Order.find(payload).then(result => {
			return result
		})

}

