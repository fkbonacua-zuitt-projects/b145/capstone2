const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	
	totalAmount : {
		type : Number,
		required : [true, "Amount is required"]
	},

	isActive : {
		type : Boolean,
		default: true
	},

	purchasedOn : {
		type : Date,
		default : new Date()
	},

	buyerName : {
		type : String,
		default : new String()
	},

	productBought : {
		type : String,
		default : new String()
	}

})

module.exports = mongoose.model('Order', orderSchema);