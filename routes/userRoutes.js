const express = require('express');

const router = express.Router();

const userController = require('../controllers/userControllers')
const auth = require('../auth');

// Registration
router.post('/register', (req, res) => {
		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))

});


// Log-in

router.post('/login', (req, res) => {
		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))

});

// Get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// Set user as admin
// router.post("/setUser", (req, res) => {

// 	const data = {
// 		userId: req.params.userId,
// 		payload: auth.decode(req.headers.authorization),
// 		setAdmin: req.body
// 	}
// 	userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
// });

router.put('/:userId', auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		payload : auth.decode(req.headers.authorization)
	}

	userController.setUserAdmin(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;