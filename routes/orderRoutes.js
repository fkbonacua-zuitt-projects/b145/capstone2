const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth')

// Add an order

router.post('/addOrder', (req, res) => {
	orderController.addOrder(req.body).then(resultFromController => res.send(resultFromController))
});

// retrieve all orders
router.get('/allOrders', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
});

// // retrieval of specific order 
// router.get('/:orderId', (req, res) => {
// 	orderController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
// });

// *
router.get('/orderList', auth.verify, (req, res) => {
	const payload = auth.decode(req.headers.authorization);

	orderController.getOrder(payload).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;