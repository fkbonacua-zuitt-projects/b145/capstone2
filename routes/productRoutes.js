const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

// Add a product

router.post('/addProduct', auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
});

// retrieval of active products
router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// get specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// update product
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization),
		updatedProduct : req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});

// archive product
router.put('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization)
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;
