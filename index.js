// [SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

// [SECTION] Environment Variables Setup
dotenv.config();
const secret = process.env.CONNNECTION_STRING;

// [SECTION] Server Setup
const port = process.env.PORT || 4000;
const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// [SECTION] Database Connect
// mongoDB connection
mongoose.connect(secret,
			{
				useNewUrlParser : true,
				useUnifiedTopology : true
			}
	);
	let db = mongoose.connection;
	db.on('error', console.error.bind(console, "Connection Error"));
	db.once('open', () => console.log("Successfully connected to the database"));

// [SECTION] Server Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// [SECTION] Server Response
app.get('/', (req, res) => {
	res.send('hosted in heroku')
})

app.listen(port, () => console.log(`Server running at port ${port}`));
